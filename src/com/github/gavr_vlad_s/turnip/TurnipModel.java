package com.github.gavr_vlad_s.turnip;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class TurnipModel extends AbstractTableModel {
    private ArrayList<TurnipActor> actors;

    public static TurnipModel standardTurnipModel(){
        ArrayList<TurnipActor> as = new ArrayList<>();
        as.add(new TurnipActor("Дедка", "Дедку",  Gender.Masculinum));
        as.add(new TurnipActor("Бабка", "Бабку",  Gender.Femininum));
        as.add(new TurnipActor("Внучка","Внучку", Gender.Femininum));
        as.add(new TurnipActor("Жучка", "Жучку",  Gender.Femininum));
        as.add(new TurnipActor("Кошка", "Кошку",  Gender.Femininum));
        as.add(new TurnipActor("Мышка", "Мышку",  Gender.Femininum));
        return new TurnipModel(as);
    }

    TurnipModel(){
        actors = new ArrayList<>();
    }

    TurnipModel(ArrayList<TurnipActor> actors){
        this.actors = actors;
    }

    void addActor(TurnipActor actor){
        actors.add(actor);
    }

    public ArrayList<TurnipActor> getActors() {
        return actors;
    }

    @Override
    public int getRowCount() {
        return actors.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TurnipActor actor  = actors.get(rowIndex);
        Object      result = null;
        switch(columnIndex){
            case 0:
                result = actor.getNominativeName();
                break;
            case 1:
                result = actor.getGenitiveName();
                break;
            case 2:
                result = actor.getGender();
                break;
        }
        return result;
    }

    @Override
    public String getColumnName(int column) {
        String result = "";
        switch(column){
            case 0:
                result = "Именительный падеж";
                break;
            case 1:
                result = "Родительный падеж";
                break;
            case 2:
                result = "Пол";
                break;
        }
        return result;
    }

    public String buildTaleText(){
        if(actors.isEmpty()){
            return "Сказка не удалась.";
        }
        TurnipActor            first = actors.get(0);
        ArrayList<TurnipActor> ta    = new ArrayList<>();
        ta.add(first);
        return prologue() + taleBody(actors, ta) + epilogue();
    }

    private String epilogue() {
        if(actors.isEmpty()){
            return "";
        }
        if(actors.size() == 1){
            TurnipActor a = actors.get(0);
            return "Вытянул" + a.ending() + " репку.";
        }
        return "Вытянули репку.";
    }

    private String prologue(){
        if(actors.isEmpty()){
            return "";
        }
        TurnipActor actor = actors.get(0);
        if(actors.size() == 1){
            return common(actor) + "Тянет-потянет.\n";
        }
        return common(actor) + "Тянет-потянет, вытянуть не может.\n";
    }

    private static final String commonFmt =
        "Посадил%s %s репку. Выросла репка большая-пребольшая. Стал%s %s репку тянуть.\n";
    private String common(TurnipActor a){
        String name   = a.getNominativeName();
        String e      = a.ending();
        return String.format(commonFmt, e, name, e, name);
    }

    private String taleBody(ArrayList<TurnipActor> as, ArrayList<TurnipActor> bs){
        if(as.size() <= 1){
            return "";
        }
        TurnipActor first       = as.get(0);
        TurnipActor second      = as.get(1);
        String      actorCalled = String.format("Позвал%s %s %s.\n",
                                                first.ending(),
                                                first.getNominativeName(),
                                                second.getGenitiveName());
        ArrayList<TurnipActor> bs1 = new ArrayList<>();
        bs1.add(second);
        bs1.addAll(bs);
        ArrayList<TurnipActor> as1 = new ArrayList<>();
        for(int i = 1; i < as.size(); ++i){
            as1.add(as.get(i));
        }
        String result = actorCalled + taleBody1(bs1) + "\n";
        if(as.size() > 2){
            result += "Вытянуть не могут. ";
        }
        result += taleBody(as1, bs1);
        return result;
    }

    private String taleBody1(ArrayList<TurnipActor> bs) {
        if(bs.isEmpty()){
            return "";
        }
        int s = bs.size();
        if(s == 1){
            TurnipActor a = bs.get(0);
            return a.getNominativeName() + " за репку. Тянут-потянут.";
        }
        String result = "";
        for(int i = 0; i < s - 1; ++i){
            result += bs.get(i).getNominativeName()   +
                      " за "                          +
                      bs.get(i + 1).getGenitiveName() +
                      ", ";
        }
        result += String.format("%s за репку. Тянут-потянут.",
                                bs.get(s - 1).getNominativeName());
        return result;
    }
}
