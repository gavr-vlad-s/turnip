package com.github.gavr_vlad_s.turnip;

public class TurnipActor {
    private final String nominativeName;
    private final String genitiveName;
    private final Gender gender;

    public TurnipActor(){
        nominativeName = "Дедка";
        genitiveName   = "Дедку";
        gender         = Gender.Masculinum;
    }

    public TurnipActor(String nominativeName, String genitiveName, Gender gender){
        this.nominativeName = nominativeName;
        this.genitiveName   = genitiveName;
        this.gender         = gender;
    }

    public Gender getGender() {
        return gender;
    }

    public String getGenitiveName() {
        return genitiveName;
    }

    public String getNominativeName() {
        return nominativeName;
    }

    @Override
    public String toString() {
        String result = nominativeName + ":" + genitiveName + ":";
        switch(gender){
            case Masculinum:
                result += "м";
                break;
            case Femininum:
                result += "ж";
                break;
            case Neutral:
                result += "с";
                break;
        }
        return result;
    }

    public String ending(){
        String result = "";
        switch(gender){
            case Femininum:
                result = "а";
                break;
            case Masculinum:
                result = "";
                break;
            case Neutral:
                result = "о";
                break;
        }
        return result;
    }
}