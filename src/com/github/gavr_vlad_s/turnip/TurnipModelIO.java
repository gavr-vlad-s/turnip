package com.github.gavr_vlad_s.turnip;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TurnipModelIO {
    public static void saveModel(String filePath, TurnipModel model){
        ArrayList<TurnipActor> actors     = model.getActors();
        List<String>           strs       = new LinkedList<>();
        for(TurnipActor a : actors){
            strs.add(a.toString());
        }
        String                 configText = String.join("\n", strs);
        try (PrintWriter pw = new PrintWriter(filePath, "UTF-8")) {
            pw.println(configText);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static TurnipModel readModel(String filePath){
        TurnipModel  model = TurnipModel.standardTurnipModel();
        Path         path  = FileSystems.getDefault().getPath(filePath);
        Charset      cs    = Charset.forName("UTF-8");
        try {
            List<String> lines = Files.readAllLines(path, cs);
            ArrayList<TurnipActor> actors = new ArrayList<>();
            for(String line : lines){
                if(!line.isEmpty()){
                    String[] actorStr = line.split(":");
                    actors.add(splitted2actor(actorStr));
                }
            }
            if(!actors.isEmpty()){
                model = new TurnipModel(actors);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return model;
    }

    private static TurnipActor splitted2actor(String[] actorStr) {
        int len = actorStr.length;
        if(len == 1){
            String  name  = actorStr[0].trim();
            boolean empty = name.isEmpty();
            return new TurnipActor(empty ? "Дедка" : name,
                                   empty ? "Дедку" : name,
                                   Gender.Masculinum);
        }
        if(len == 2){
            String  nominative      = actorStr[0].trim();
            boolean emptyNominative = nominative.isEmpty();
            String  genitive        = actorStr[1].trim();
            boolean emptyGenitive   = genitive.isEmpty();
            return new TurnipActor(emptyNominative ? "Дедка" : nominative,
                                   emptyGenitive   ? "Дедку" : genitive,
                                   Gender.Masculinum);
        }
        if(len >= 3){
            String  nominative      = actorStr[0].trim();
            boolean emptyNominative = nominative.isEmpty();
            String  genitive        = actorStr[1].trim();
            boolean emptyGenitive   = genitive.isEmpty();
            String  genderStr       = actorStr[2].trim();
            return new TurnipActor(emptyNominative ? "Дедка" : nominative,
                                   emptyGenitive   ? "Дедку" : genitive,
                                   str2gender(genderStr));
        }
        return new TurnipActor();
    }

    private static Gender str2gender(String genderStr) {
        if(genderStr.isEmpty()){
            return Gender.Masculinum;
        }
        Gender gender;
        switch(genderStr.codePointAt(0)){
            case 'ж': case 'Ж':
                gender = Gender.Femininum;
                break;
            case 'м': case 'М':
                gender = Gender.Masculinum;
                break;
            case 'с': case 'С':
                gender = Gender.Neutral;
                break;
            default:
                gender = Gender.Masculinum;
        }
        return gender;
    }
}
