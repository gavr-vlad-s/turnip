package com.github.gavr_vlad_s.turnip;

import javax.swing.*;
import java.awt.*;

public class AboutDialog extends JDialog {
    private static final String tags           =
        "<html><h3><i>Сказка про репку.</i></h3>" +
        "<hr>(c) Гаврилов В.С., 2019</html>";
    private static final int    DEFAULT_WIDTH  = 250;
    private static final int    DEFAULT_HEIGHT = 150;

    public AboutDialog(JFrame owner){
        super(owner, "О программе", true);
        add(new JLabel(tags), BorderLayout.CENTER);
        JPanel  panel = new JPanel();
        JButton ok    = new JButton("ОК");
        ok.addActionListener(e -> setVisible(false));
        panel.add(ok);
        add(panel, BorderLayout.SOUTH);
        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }
}
