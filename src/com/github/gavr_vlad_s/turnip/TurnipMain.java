package com.github.gavr_vlad_s.turnip;

import javax.swing.*;
import java.awt.*;

public class TurnipMain {
    public static void main(String[] args) {
        EventQueue.invokeLater(() ->{
            JFrame frame = new TurnipFrame();
            frame.setTitle("Конфигурируемая сказка 'Репка'");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
            //frame.setResizable(false);
        });
    }
}
