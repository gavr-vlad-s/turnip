package com.github.gavr_vlad_s.turnip;

public enum Gender{
    Masculinum, Femininum, Neutral;

    private final static String[] names = new String[]{"м", "ж", "с"};

    @Override
    public String toString() {
        return names[ordinal()];
    }
}
