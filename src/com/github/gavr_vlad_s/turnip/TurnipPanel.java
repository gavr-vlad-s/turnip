package com.github.gavr_vlad_s.turnip;

import javax.swing.*;
import java.awt.*;

public class TurnipPanel extends JPanel {
    private TurnipModel taleModel;
    private JTable      table;
    private JPanel      tablePanel;
    private JPanel      outputPanel;
    private JTextArea   taleTextArea;

    public TurnipPanel(){
        taleModel    = TurnipModel.standardTurnipModel();
        table        = new JTable(taleModel);
        setLayout(new BorderLayout());
        tablePanel   = new JPanel();
        outputPanel  = new JPanel();
        tablePanel.setLayout(new BorderLayout());
        outputPanel.setLayout(new BorderLayout());
        tablePanel.add(new JScrollPane(table), BorderLayout.CENTER);
        taleTextArea = new JTextArea(10, 50);
        outputPanel.add(new JScrollPane(taleTextArea), BorderLayout.CENTER);
        add(tablePanel, BorderLayout.NORTH);
        add(outputPanel, BorderLayout.SOUTH);

        String text = taleModel.buildTaleText();
        taleTextArea.setText(text);
    }

    public TurnipModel getModel() {
        return taleModel;
    }

    public void setModel(TurnipModel taleModel) {
        this.taleModel = taleModel;
        showModel();
    }

    private void showModel(){
        table.setModel(taleModel);
        String text = taleModel.buildTaleText();
        taleTextArea.setText(text);
    }
}
