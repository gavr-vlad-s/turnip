package com.github.gavr_vlad_s.turnip;

import javax.swing.*;
import java.io.File;

public class TurnipFrame extends JFrame {
    private TurnipPanel  panel;
    private JFileChooser chooser;

    TurnipFrame(){
        panel              = new TurnipPanel();
        chooser            = new JFileChooser();
        JMenu    fileMenu  = new JMenu("Файл");

        JMenuItem openItem = new JMenuItem("Открыть");
        openItem.addActionListener(e -> {
            chooser.setCurrentDirectory(new File("."));
            int result = chooser.showOpenDialog(TurnipFrame.this);
            if(result == JFileChooser.APPROVE_OPTION){
                String      fileName = chooser.getSelectedFile().getPath();
                TurnipModel model    = TurnipModelIO.readModel(fileName);
                panel.setModel(model);
            }
        });
        JMenuItem saveItem = new JMenuItem("Сохранить");
        saveItem.addActionListener(e -> save());
        JMenuItem exitItem = new JMenuItem("Выход");
        exitItem.addActionListener(e -> {
            boolean neededSaving = requestSaving();
            if(neededSaving){
                save();
            }
            System.exit(0);
        });
        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(exitItem);

        JMenuItem aboutItem = new JMenuItem("О программе");
        aboutItem.addActionListener(e -> {
            JDialog dialog = new AboutDialog(this);
            dialog.setVisible(true);
        });
        JMenu  helpMenu    = new JMenu("Справка");
        helpMenu.add(aboutItem);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);
        setJMenuBar(menuBar);

        add(panel);
        pack();
    }

    private static final String  questionMsg =
            "При выходе данные будут потеряны. Хотите сохранить?";
    private static final String  questionTitle = "Запрос сохранения";

    private boolean requestSaving() {
        int selection = JOptionPane.showConfirmDialog(this,
                questionMsg,
                questionTitle,
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        return selection == JOptionPane.OK_OPTION;
    }

    private void save(){
        chooser.setCurrentDirectory(new File("."));
        int result = chooser.showSaveDialog(TurnipFrame.this);
        if(result == JFileChooser.APPROVE_OPTION){
            String    fileName = chooser.getSelectedFile().getPath();
            TurnipModel model  = panel.getModel();
            TurnipModelIO.saveModel(fileName, model);
        }
    }

}
